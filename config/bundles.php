<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle::class => ['all' => true],
    FreedomSex\RestMachineBundle\RestMachineBundle::class => ['dev' => true, 'test' => true],
];
