<?php

namespace FreedomSex\RestMachineBundle\Services;

use Symfony\Contracts\HttpClient\ResponseInterface;

trait RequestsTrait
{
    public function get(?array $params = null, ?string $url = null, ?int $wait = null): ?ResponseInterface
    {
        return $this->request("GET", "get", null, $params, $url, $wait);
    }

    public function load(?array $params = null, ?string $url = null, ?int $wait = null): ?ResponseInterface
    {
        return $this->request("GET", "load", null, $params, $url, $wait);
    }

    public function cget(?array $params = null, ?string $url = null, ?int $wait = null): ?ResponseInterface
    {
        return $this->request("GET", "cget", null, $params, $url, $wait);
    }

    public function send(?array $params = null, ?string $url = null, ?int $wait = null): ?ResponseInterface
    {
        return $this->request("GET", "send", null, $params, $url, $wait);
    }

    public function post(?array $data = null, ?array $params = null, ?string $url = null, ?int $wait = null): ?ResponseInterface
    {
        return $this->request("POST", "post", $data, $params, $url, $wait);
    }

    public function new(?array $data = null, ?array $params = null, ?string $url = null, ?int $wait = null): ?ResponseInterface
    {
        return $this->request("POST", "new", $data, $params, $url, $wait);
    }

    public function edit(?array $data = null, ?array $params = null, ?string $url = null, ?int $wait = null): ?ResponseInterface
    {
        return $this->request("POST", "edit", $data, $params, $url, $wait);
    }

    public function remove(?array $data = null, ?array $params = null, ?string $url = null, ?int $wait = null): ?ResponseInterface
    {
        return $this->request("POST", "remove", $data, $params, $url, $wait);
    }

    public function delete(?array $data = null, ?array $params = null, ?string $url = null, ?int $wait = null): ?ResponseInterface
    {
        return $this->request("POST", "delete", $data, $params, $url, $wait);
    }

    public function patch(?array $data = null, ?array $params = null, ?string $url = null, ?int $wait = null): ?ResponseInterface
    {
        $data = $data ?: [];
        return $this->request("PATCH", "patch", $data, $params, $url, $wait);
    }

    public function put(?array $data = null, ?array $params = null, ?string $url = null, ?int $wait = null): ?ResponseInterface
    {
        $data = $data ?: [];
        return $this->request("PUT", "put", $data, $params, $url, $wait);
    }

    public function option(?array $params = null, ?string $url = null, ?int $wait = null): ?ResponseInterface
    {
        return $this->request("OPTION", "option", null, $params, $url, $wait);
    }

    public function isDataRequest($method, ?array $data = null)
    {
        return $data or in_array($method, ['post', 'put', 'patch']);
    }

    public function waitBeforeResponse($configuredDelay, ?int $waitResponse = 0)
    {
        $seconds = $waitResponse;
        if ($configuredDelay >= $waitResponse) {
            $seconds = $configuredDelay;
        }
        if ($seconds) {
            sleep($seconds);
        }
    }

    abstract public function request(
        string $method,
        string $action,
        ?array $data = null,
        ?array $params = null,
        ?string $url = null,
        ?int $wait = null
    ): ResponseInterface;

}
