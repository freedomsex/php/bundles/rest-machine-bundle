<?php

namespace FreedomSex\RestMachineBundle\Services;

class Router
{
    public const DEFAULT_ROUTES = [
        'get' => '{id}',
        'cget' => '',
        'post' => '',
        'delete' => '{id}',
        'put' => '{id}',
        'patch' => '{id}',
        'option' => '{id}',
        'load' => '',
        'send' => '',
        'new' => 'new',
        'save' => 'save',
        'edit' => 'edit/{id}',
        'remove' => 'remove/{id}',
        'route' => '',
    ];

    public $url = '';
    public $root = '/';
    public $params = [];
    public $version;
    public $prefix;
    public $routing;
    public $wait;

    public function __construct()
    {
        $this->routing = self::DEFAULT_ROUTES;
    }

    public function initialize(?string $host = '', ?string $prefix = '', ?string $version = '')
    {
        $ver = $version ? "$version/" : '';
        $pre = $prefix ? "$prefix/" : '';
        $this->root = "$host/$pre$ver";
        $this->params = [];
        $this->url = '';

        return $this;
    }

    public function reset()
    {
        $this->params = [];
        $this->wait = 0;
        $this->url = '';
    }

    public function setRouting(array $routing = [], ?string $resource = null)
    {
        $this->routing = [
            'route' => $resource
        ];
        $this->routing = array_merge(
            self::DEFAULT_ROUTES,
            $this->routing,
            $routing
        );
        return $this;
    }

    public function setParams(?array $params = null, ?string $url = null)
    {
        if (!$url or !$params) {
            return $url;
        }
        $result = preg_replace_callback('/\{(.*?)\}/i', function ($matches) use (&$params) {
            $token = $matches[1];
            $slug = $params[$token];
            unset($params[$token]);
            return $slug;
        }, $url);
        $this->params = $params ?: [];
        return $result;
    }

    public function getUrlPath(string $method)
    {
        $route = $this->routing['route'];
        $action = $this->routing[$method];
        $result = $route ?: '';
        if ($result and $action) {
            $result = "$result/$action";
        } elseif ($action) {
            $result = $action;
        }
        return $result;
    }

    public function setUrl(string $method, ?array $params = null, ?string $url = null)
    {
        if ($this->isAbsolute($url)) {
            $result = $url;
            $this->root = '';
        } else {
            if ($url) {
                $result = $url;
            } else {
                $result = $this->getUrlPath($method);
            }
        }
        $result = $this->setParams($params, $result);
        return $this->root . $result;
    }

    public function isAbsolute(?string $url = null): bool
    {
        return preg_match('(www|://)', $url) === 1;
    }

    public function setWaitTime(?int $seconds = 0)
    {
        $this->wait = $seconds;

        return $this;
    }

}
