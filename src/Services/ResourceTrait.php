<?php

namespace FreedomSex\RestMachineBundle\Services;

trait ResourceTrait
{
    public array $resources = [];

    public function setResource($name, $resource)
    {
        $this->resources[$name] = $resource;
    }

    public function setResources(array  $resources)
    {
        $this->resources = $resources;
    }

    public function arrayWithoutNullValues(array $resource): array
    {
        return array_filter($resource, fn ($value) => !is_null($value));
    }

    public function extendResource($name, ?array $resource = [])
    {
        $resource = $this->arrayWithoutNullValues($resource);
        $this->resources[$name] = array_merge($this->resources[$name] ?? [], $resource);
    }

    public function getConfig($name): array
    {
        $config = $this->resources[$name] ?? [];
        $config = $this->arrayWithoutNullValues($config);
        return array_merge($this->resources['default'] ?? [], $config);
    }

}
