<?php

namespace FreedomSex\RestMachineBundle\Services;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class AuthReflector
{
    private Request $request;

    public function __construct(
        RequestStack $requestStack
    ) {
        $this->request = $requestStack->getCurrentRequest();
    }

    public function reflectXLoginHeaders()
    {
        return [
            'X-Login' => $this->request->headers->get('X-Login'),
            'X-Login-ID' => $this->request->headers->get('X-Login-ID'),
        ];
    }

    public function reflectAuthorizationHeaders()
    {
        return [
            'Authorization' => $this->request->headers->get('Authorization'),
        ];
    }

    public function reflect(string $reflector = 'authorization')
    {
        if ($reflector == 'authorization') {
            return $this->reflectAuthorizationHeaders();
        }
        if ($reflector == 'x-login') {
            return $this->reflectXLoginHeaders();
        }
    }
}
