<?php

namespace FreedomSex\RestMachineBundle\Services;

class ApiResource
{
    public RestClient $restClient;
    private AuthReflector $authReflector;

    public function __construct(
        RestClient $requests,
        AuthReflector $authReflector
    ) {
        $this->restClient = $requests;
        $this->authReflector = $authReflector;
    }

    public function res($resourceName, ?string $apiName = null): RestClient
    {
        $this->initializeApiHostConfig($apiName ?? $resourceName);
        return $this->initializeApiResource($resourceName);
    }

    public function initializeApiHostConfig($name)
    {
        $config = $this->restClient->getConfig($name);
        $this->restClient->router->initialize(
            $config['host'] ?? '',
            $config['prefix'] ?? '',
            $config['version'] ?? '',
        );
        if (!$this->restClient->isAuth()) {
            $this->restClient->setAuthKey($config['key'] ?? null);
        }
        $this->restClient->router->setWaitTime($config['wait'] ?? 0);

        return $this;
    }

    public function initializeApiResource($name): RestClient
    {
        $config = $this->restClient->getConfig($name);
        $this->restClient->router->setRouting($config['routing'] ?? [], $name);
        return $this->restClient;
    }

    public function reflectAuth(string $reflector = 'authorization'): self
    {
        $headers = $this->authReflector->reflect($reflector);
        $this->restClient->setHeaders($headers);
        return $this;
    }

//    public function baseUrl()
//    {
//
//    }
//
    public function lastUrl()
    {
        return $this->restClient->lastUrl();
    }

}
