<?php

namespace FreedomSex\RestMachineBundle\Services;

use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class RestClient
{
    use RequestsTrait;
    use ResourceTrait;
    use RestClientTrait;

    public Router $router;

    private HttpClientInterface $client;

    public array $resources = [];

    public function __construct(
        Router $router,
        HttpClientInterface $client,
        ?array $resources = []
    ) {
        $this->resources = $resources;
        $this->router = $router;
        $this->client = $client;
    }

    public function setClient(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function saveAuthKey($key, $name)
    {
        $this->extendResource($name, ['key' => $key]);
    }

    public function setAuthKey(string $key = null, string $name = null): self
    {
        $authKey = $key ?: $this->defaults['key'] ?? null;
        if ($authKey) {
            $this->setHeaders(['Authorization' => "Bearer $authKey"]);
        }
        if ($name) {
            $this->saveAuthKey($authKey, $name);
        }
        return $this;
    }

    public function wait($seconds)
    {
        $this->router->setWaitTime($seconds);

        return $this;
    }

    public function request(
        string $method,
        string $action,
        ?array $data = null,
        ?array $params = null,
        ?string $url = null,
        ?int $wait = null
    ): ?ResponseInterface {
        $response = null;
        $uri = $this->router->setUrl($action, $params, $url);
        $this->options['query'] = $this->router->params;
        if ($this->isDataRequest($method, $data)) {
            $this->options['json'] = $data;
        }
        if (!$this->onlyGenerateUrl) {
            $response = $this->client->request($method, $uri, $this->options);
            $this->waitBeforeResponse($this->router->wait, $wait);
        }
        $this->lastGeneratedUrl = $uri;
        $this->reset();

        return $response;
    }
}
