<?php

namespace FreedomSex\RestMachineBundle\Services;

trait RestClientTrait
{
    private $onlyGenerateUrl = false;
    private $lastGeneratedUrl = false;

    public array $defaults = [
        'key' => null,
        'base_uri' => null,
    ];
    public array $options = [
        'headers' => [],
        'base_uri' => null,
    ];

    public function onlyUrl()
    {
        $this->onlyGenerateUrl = true;

        return $this;
    }

    public function lastUrl()
    {
        return $this->lastGeneratedUrl;
    }

    public function reset()
    {
        $this->options = [
            'headers' => [],
            'base_uri' => null,
        ];
        $this->router->reset();
        $this->onlyGenerateUrl = false;
    }

    public function setBaseURL($url)
    {
        $this->defaults['base_uri'] ??= null;
        $base = $url ?: $this->defaults['base_uri'] ?: null;
        if ($base) {
            $this->options['base_uri'] = $base;
        }
    }

    public function setHeaders($headers)
    {
        if ($headers) {
            $this->options['headers'] = array_merge($this->options['headers'], $headers);
        }
    }

    public function default($key, $val)
    {
        if ($val !== null) {
            $this->defaults[$key] = $val;
        }
        return $this->defaults[$key];
    }

    public function isAuth(): bool
    {
        // $this->options['headers']['Authorization']
        return (bool) $this->options['headers'];
    }

    public function setOptions(array $options = [])
    {
        $this->options = array_merge($this->options, $options);
    }

}
