<?php

namespace FreedomSex\RestMachineBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('rest_machine');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
                ->arrayNode('resources')
                    ->useAttributeAsKey('name')
                    ->arrayPrototype()
                        ->addDefaultsIfNotSet()
                        ->children()
                            ->scalarNode('host')->defaultNull()->end()
                            ->scalarNode('prefix')->defaultNull()->end()
                            ->scalarNode('version')->defaultNull()->end()
                            ->scalarNode('key')->defaultNull()->end()
                            ->scalarNode('wait')->defaultNull()->end()
                            ->arrayNode('routing')
                                ->addDefaultsIfNotSet()
                                ->children()
                                    ->scalarNode('get')->defaultValue('{id}')->end()
                                    ->scalarNode('cget')->defaultValue('')->end()
                                    ->scalarNode('post')->defaultValue('')->end()
                                    ->scalarNode('delete')->defaultValue('{id}')->end()
                                    ->scalarNode('put')->defaultValue('{id}')->end()
                                    ->scalarNode('patch')->defaultValue('{id}')->end()
                                    ->scalarNode('option')->defaultValue('{id}')->end()
                                    ->scalarNode('load')->defaultValue('')->end()
                                    ->scalarNode('send')->defaultValue('')->end()
                                    ->scalarNode('new')->defaultValue('new')->end()
                                    ->scalarNode('save')->defaultValue('save')->end()
                                    ->scalarNode('edit')->defaultValue('edit/{id}')->end()
                                    ->scalarNode('remove')->defaultValue('remove/{id}')->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }

}
