# Rest Machine bundle

Ported from https://www.npmjs.com/package/axios-rest-api
 
```yaml
# /config/packages/rest_machine.yaml
rest_machine:
    resources:
        default:
            prefix: 'api'
            host: ''
        search:
            host: '%env(API_SEARCH_HOST)%'
        access:
            host: '%env(API_ACCESS_HOST)%'
```

```dotenv
#/.env

API_SEARCH_HOST=http://127.0.0.1:8007
API_ACCESS_HOST=http://127.0.0.1:8001

```

## Get Service

**Public ID**: `rest_machine.api_resource`  
**Class**: _FreedomSex\RestMachineBundle\Services\ApiResource_

### Usage

```php
use FreedomSex\RestMachineBundle\Services\ApiResource;
# ...
    public function __construct(
        ApiResource $api
    ) {
        $this->api = $api;
    }

    public function test()
    {
        // get `resource` Users from API service `search` with ID: 1000
        // API service `search` configured for example to host `http://127.0.0.1:8007` 
        // Resulting request URL `http://127.0.0.1:8007/api/users/1000` with method GET
        $result = $this->api->res('users', 'search')->get(['id' => '1000']);
        print_r($result);
    }
    
    
# ...


```

---

**onlyUrl()** `$this->api->res('users')->onlyUrl()->load();` - не вызывает ресурс, a только генерирует URL.
Запрос вернет NULL. URL можно получить вызвав метод `$this->api->lastUrl()`


