# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.2.7](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.2.6...1.2.7) (2022-12-20)


### Bug Fixes

* lastUrl ([162559a](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/162559ade429f94f0e38fa3453f25b2ac64fe6a0))

### [1.2.6](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.2.5...1.2.6) (2022-12-20)


### Bug Fixes

* reset wait ([b9944bc](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/b9944bcf30f37be955c6206a0e69c04135527c8f))

### [1.2.5](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.2.4...1.2.5) (2022-12-20)


### Bug Fixes

* reset request ([79486c5](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/79486c5ee2e454e57ed797a4cd2381b950c8e985))

### [1.2.4](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.2.3...1.2.4) (2022-12-20)


### Bug Fixes

* reset params ([3d07e9a](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/3d07e9a1f11305c4bcf18dee0858f978981343eb))

### [1.2.3](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.2.2...1.2.3) (2022-12-06)


### Bug Fixes

* wait action ([9e701f4](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/9e701f45debbf886441420dcbda5718ea53ffc87))

### [1.2.2](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.2.1...1.2.2) (2022-12-06)


### Bug Fixes

* wait chain ([130fdaf](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/130fdaf2cc8860e2cdf2e2aaea7993806eff9520))

### [1.2.1](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.2.0...1.2.1) (2022-12-06)


### Bug Fixes

* wait config ([56879c6](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/56879c679a7510bcfdac7e91b11f6ba969fba535))

## [1.2.0](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.1.2...1.2.0) (2022-12-06)


### Features

* waitBeforeResponse ([eb245f6](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/eb245f61711a77ee4481ac2ee20901aa47a0c20b))

### [1.1.2](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.1.1...1.1.2) (2022-12-05)


### Bug Fixes

* put request ([787ca8e](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/787ca8e12a1fda805cc47c71689160d595375140))

### [1.1.1](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.1.0...1.1.1) (2022-12-05)


### Bug Fixes

* merge arrayWithoutNullValues ([56aa634](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/56aa634cfff4840173a1b8ce2a409652fbea68a3))
* return array ([b4843e8](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/b4843e8d635ed5f24a31def84b3fc32c9928f687))

## [1.1.0](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.0.17...1.1.0) (2022-12-05)


### Features

* docs ([a721216](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/a72121617829a56f9c18522f50440bcdcdab0184))

### [1.0.17](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.0.16...1.0.17) (2022-12-05)


### Bug Fixes

* merge defaults ([8e7c809](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/8e7c80992d50b8940a0d03ee729dd43500f94d8f))

### [1.0.16](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.0.15...1.0.16) (2022-12-05)


### Bug Fixes

* req+ ([5f8a519](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/5f8a5194bc66bc93ea47a05a01328f6a6727db41))

### [1.0.15](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.0.14...1.0.15) (2022-12-05)


### Bug Fixes

* req ([91f837a](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/91f837aa468d8ba61a42beab51996c28aabbcd54))

### [1.0.14](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.0.13...1.0.14) (2022-09-29)


### Bug Fixes

* requests ([84a8448](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/84a84487829617a241d4782352389b290f30a6c7))

### [1.0.13](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.0.12...1.0.13) (2022-09-29)


### Bug Fixes

* headers ([0cab0d5](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/0cab0d54612b722c410f009848d0a6fa08fc3b76))

### [1.0.12](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.0.11...1.0.12) (2022-09-29)


### Bug Fixes

* options ([0430640](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/04306404ad0440f2a57a59795d17bdb9ea18ceed))

### [1.0.11](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.0.10...1.0.11) (2022-09-29)


### Bug Fixes

* return self ([9b895c0](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/9b895c0df2b203da759bf3bc733f3683b6ae30ce))

### [1.0.10](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.0.9...1.0.10) (2022-09-29)


### Bug Fixes

* reflector ([8e8e83c](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/8e8e83c49143465e361d40f646687cf679be911d))

### [1.0.9](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.0.8...1.0.9) (2022-09-29)


### Bug Fixes

* return ([68fcc72](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/68fcc72aeceea1b799b55332773d7872b86017c0))

### [1.0.8](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.0.7...1.0.8) (2022-09-29)


### Bug Fixes

* reflect auth ([f8ee721](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/f8ee72109c18892e0059cecd95aea8b764ee0ad9))

### [1.0.7](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.0.6...1.0.7) (2022-09-29)


### Bug Fixes

* root slash ([5d0f8a9](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/5d0f8a9a0465ede9bb44cc262a3785ac09afa605))

### [1.0.6](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.0.5...1.0.6) (2022-09-29)


### Bug Fixes

* router init ([2fb5d5a](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/2fb5d5a8d1669656789f605cabb50c16d7aab944))

### [1.0.5](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.0.4...1.0.5) (2022-09-29)


### Bug Fixes

* findDefinition ([61ad949](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/61ad949efd3eb08cab10cb94bb7f7f700514bcfa))

### [1.0.4](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.0.3...1.0.4) (2022-09-29)


### Bug Fixes

* service ([f93296c](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/f93296c4955f14aa7af5a4a24a2106bfc6074395))

### [1.0.3](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.0.2...1.0.3) (2022-09-29)


### Bug Fixes

* traits ([2cc9272](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/2cc927210530ec538bf00675598f3f0347bba6b5))

### [1.0.2](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/compare/1.0.1...1.0.2) (2022-09-27)


### Bug Fixes

* replace ([4d8c399](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/4d8c39989788f97732bbfe82c805b5a420dfcac8))
* ver ([ed55d32](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/ed55d3292296178125ede932f5f13ca723af7852))

### 1.0.1 (2022-09-27)


### Bug Fixes

* read ([5f4d4a2](https://gitlab.com/freedomsex/php/bundles/rest-machine-bundle/commit/5f4d4a2e4470bb4d9372d3ded2fb5a5eab931c48))
