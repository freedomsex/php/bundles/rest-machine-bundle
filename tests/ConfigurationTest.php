<?php

namespace FreedomSex\RestMachineBundle\Tests;

use FreedomSex\RestMachineBundle\DependencyInjection\Configuration;

class ConfigurationTest extends BaseTestSetUp
{
    public function setUp():void
    {
        parent::setUp();
    }

    public function testGetConfigWithOverrideValues()
    {
        $configs = [
            'resources' => [
                [
                    'name' => 'users',
                    'host' => 'localhost',
                    'prefix' => 'api',
                ],
            ],
        ];

        $configuration = new Configuration();

        $node = $configuration->getConfigTreeBuilder()
            ->buildTree();
        $normalizedConfig = $node->normalize($configs);
        $finalizedConfig = $node->finalize($normalizedConfig);
        $this->assertEquals('localhost', $finalizedConfig['resources']['users']['host']);
    }

    public function testTestConfigYaml()
    {
        $this->assertTrue($this->getContainer()->has('rest_machine.rest_client'));
        $client = $this->get('rest_machine.rest_client');
//        print_r($client->getConfig('test'));
//        print_r($client->resources);
    }

}
