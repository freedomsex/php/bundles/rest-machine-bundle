<?php

namespace FreedomSex\RestMachineBundle\Tests\Services;

use FreedomSex\RestMachineBundle\Services\RestClient;
use FreedomSex\RestMachineBundle\Tests\BaseTestSetUp;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\Stopwatch\Stopwatch;

class RestClientTest extends BaseTestSetUp
{
    private RestClient $object;

    protected function setUp(): void
    {
        parent::setUp();
        $this->object = $this->get(RestClient::class);
        $this->mockClient = new MockHttpClient([new MockResponse('', [
            'http_method' => 'POST'
        ])]);
    }

    public function testIsDataRequest()
    {
        self::assertTrue($this->object->isDataRequest('POST', [1]));
        self::assertFalse($this->object->isDataRequest('GET'));
        self::assertTrue($this->object->isDataRequest('GET', [1]));
    }

    public function testRequest()
    {
        $this->object->setClient($this->mockClient);
        $this->object->router->initialize();

        $response = $this->object->request('POST', 'edit', null, ['id' => 12345], 'http://localhost/edit/{id}');
        self::assertEquals('POST', $response->getInfo()['http_method']);
    }

    public function testSetAuthKey()
    {
        $this->object->setAuthKey('12345', 'users');
        self::assertEquals('12345', $this->object->resources['users']['key']);
        self::assertEquals('Bearer 12345', $this->object->options['headers']['Authorization']);
    }

    public function testWait()
    {
        $this->object->setClient($this->mockClient);
        $this->object->router->initialize();

        $this->object->wait('1');
        self::assertEquals('1', $this->object->router->wait);
        $time = date('i:s');
        $this->object->request('POST', 'load', null, null, 'http://localhost/users');
        self::assertNotEquals($time, date('i:s'));
//        self::assertEquals('Bearer 12345', $this->object->options['headers']['Authorization']);
    }
}
