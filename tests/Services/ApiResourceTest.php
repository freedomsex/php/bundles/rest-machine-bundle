<?php

namespace FreedomSex\RestMachineBundle\Tests\Services;

use FreedomSex\RestMachineBundle\Services\ApiResource;
use FreedomSex\RestMachineBundle\Tests\BaseTestSetUp;

class ApiResourceTest extends BaseTestSetUp
{
    private ApiResource $object;

    protected function setUp(): void
    {
        parent::setUp();
        $this->object = $this->get(ApiResource::class);
        $this->resource = [
            'host' => 'http://localhost',
            'prefix' => 'api',
            'version' => 'v1',
        ];
    }

    public function testInitializeApi()
    {
        $this->object->restClient->setResource('users', $this->resource);
        $this->object->initializeApiHostConfig('users');
        self::assertEquals('http://localhost/api/v1/', $this->object->restClient->router->root);
    }

    public function testInitializeResource()
    {
        $this->object->restClient->setResource('users', $this->resource);
        $this->object->initializeApiResource('users');
        self::assertEquals('users', $this->object->restClient->router->routing['route']);
    }

    public function testRes()
    {
        $this->object->restClient->setResource('users', $this->resource);
        $this->object->res('users');
        self::assertEquals('http://localhost', $this->object->restClient->getConfig('users')['host']);
        self::assertEquals('http://localhost/api/v1/', $this->object->restClient->router->root);
    }

    public function testResApi()
    {
        $this->object->restClient->setResource('users', $this->resource);
        $apiResource = $this->object->res('rank', 'users');
        self::assertEquals('http://localhost', $apiResource->getConfig('users')['host']);
        self::assertEquals('http://localhost/api/v1/', $apiResource->router->root);
    }

    public function testResetParams()
    {
        $this->object->restClient->setResource('users', $this->resource);
        $apiResource = $this->object->res('rank', 'users');
        $apiResource->onlyUrl()->get(['id' => 1]);
        self::assertEquals('http://localhost/api/v1/rank/1', $this->object->lastUrl());
        $apiResource->onlyUrl()->post(['id' => 1, 'test' => 'test']);
        self::assertEquals([], $apiResource->router->params);
        self::assertEquals('http://localhost/api/v1/rank', $this->object->lastUrl());
        self::assertEquals([], $apiResource->router->params);
    }

    public function testReflectAuth()
    {
        $this->loginUser(1000);
        $headers = $this->object->reflectAuth('x-login')->restClient->options['headers'];
        self::assertArrayHasKey('X-Login', $headers);
    }

}
