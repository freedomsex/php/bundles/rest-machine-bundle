<?php

namespace FreedomSex\RestMachineBundle\Tests\Services;

use FreedomSex\RestMachineBundle\Services\RestClient;
use FreedomSex\RestMachineBundle\Services\ResourceTrait;
use FreedomSex\RestMachineBundle\Tests\BaseTestSetUp;

class ResourceTraitTest extends BaseTestSetUp
{
    /** @var ResourceTrait|object */
    private $object;

    protected function setUp(): void
    {
        parent::setUp();
        $this->object = $this->getObjectForTrait(ResourceTrait::class);
        $this->resource = [
            'host' => 'localhost',
            'prefix' => 'api',
            'version' => 'v1',
        ];
    }

    public function testSetResource()
    {
        $this->object->setResource('users', ['key' => '12345']);
        self::assertEquals('12345', $this->object->resources['users']['key']);
        $this->object->setResource('users', $this->resource);
        self::assertEquals('localhost', $this->object->resources['users']['host']);
    }

    public function testGetConfig()
    {
        $this->object->setResource('users', $this->resource);
        self::assertEquals('localhost', $this->object->getConfig('users')['host']);
    }

    public function testExtendResource()
    {
        $this->object->extendResource('users', ['key' => '12345']);
        self::assertEquals($this->object->resources['users']['key'], '12345');
    }
}
