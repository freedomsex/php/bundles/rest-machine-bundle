<?php

namespace FreedomSex\RestMachineBundle\Tests\Services;

use FreedomSex\RestMachineBundle\Services\ApiResource;
use FreedomSex\RestMachineBundle\Tests\BaseTestSetUp;

class ApiResourceDefaultTest extends BaseTestSetUp
{
    private ApiResource $object;

    protected function setUp(): void
    {
        parent::setUp();
        $this->object = $this->get(ApiResource::class);
        $this->resources = [
            'default' => [
                'prefix' => 'api',
                'host' => null,
            ],
            'users' => [
                'host' => 'http://localhost',
            ]
        ];
    }

    public function testRes()
    {
        $this->object->restClient->setResources($this->resources);
        $resource = $this->object->res('users');
        self::assertEquals('http://localhost/api/users/1', $this->object->restClient->router->setUrl('get', ['id' => 1]));
//        self::assertEquals('http://localhost/api/', $resource->);
    }

    public function testConfig()
    {
        $resource = $this->object->res('test');
        self::assertEquals('http://localhost/api/test/1', $this->object->restClient->router->setUrl('get', ['id' => 1]));
    }

    public function testApiName()
    {
        $resource = $this->object->res('users', 'test');
        self::assertEquals('http://localhost/api/users/1', $this->object->restClient->router->setUrl('get', ['id' => 1]));
    }

}
