<?php

namespace FreedomSex\RestMachineBundle\Tests\Services;

use FreedomSex\RestMachineBundle\Services\RestClientTrait;
use FreedomSex\RestMachineBundle\Tests\BaseTestSetUp;
use PHPUnit\Framework\TestCase;

class RestClientTraitTest extends BaseTestSetUp
{
    /** @var RestClientTrait|object */
    private $object;

    protected function setUp(): void
    {
        parent::setUp();
        $this->object = $this->getObjectForTrait(RestClientTrait::class);
    }

    public function testIsAuth()
    {
        self::assertFalse($this->object->isAuth());
    }

    public function testDefault()
    {
        self::assertEquals('12345', $this->object->default('key', '12345'));
    }

    public function testSetBaseURL()
    {
        $this->object->setBaseURL('localhost');
        self::assertEquals('localhost', $this->object->options['base_uri']);
    }

    public function testSetBaseURLDefault()
    {
        $this->object->default('base_uri', 'localhost');
        $this->object->setBaseURL(null);
        self::assertEquals('localhost', $this->object->options['base_uri']);
    }

    public function testSetHeaders()
    {
        $this->object->setHeaders(['a' => 'b']);
        self::assertEquals('b', $this->object->options['headers']['a']);
    }
}
