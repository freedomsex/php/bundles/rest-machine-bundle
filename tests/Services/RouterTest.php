<?php

namespace FreedomSex\RestMachineBundle\Tests\Services;

use FreedomSex\RestMachineBundle\Services\Router;
use FreedomSex\RestMachineBundle\Tests\BaseTestSetUp;

class RouterTest extends BaseTestSetUp
{
    private Router $object;

    protected function setUp(): void
    {
        parent::setUp();
        $this->object = $this->get(Router::class);
    }

    public function testInit()
    {
        $this->object->initialize('', '', '');
        self::assertEquals('/', $this->object->root);
        $this->object->initialize('', '', 'v1');
        self::assertEquals('/v1/', $this->object->root);
        $this->object->initialize('', 'api', 'v1');
        self::assertEquals('/api/v1/', $this->object->root);

        $this->object->initialize();
        $this->object->initialize('');
        $this->object->initialize('', '');

    }

    public function testSetRouting()
    {
        $this->object->setRouting([], 'users');
        self::assertEquals('users', $this->object->routing['route']);
        $this->object->setRouting(['get' => ''], 'users');
        self::assertEquals('', $this->object->routing['get']);
    }

    public function testSetParams()
    {
        self::assertEquals('/users/12345', $this->object->setParams(['id' => 12345], '/users/{id}'));
    }

    public function testGetUrlPath()
    {
        $this->object->initialize('', '', '');
        $this->object->setRouting([], 'users');
        self::assertEquals('users/edit/{id}', $this->object->getUrlPath('edit'));
    }

    public function testIsAbsolute()
    {
        self::assertTrue($this->object->isAbsolute('https://php.net'));
        self::assertFalse($this->object->isAbsolute('phpnet'));
        self::assertFalse($this->object->isAbsolute('php.net'));
        self::assertFalse($this->object->isAbsolute('/phpnet'));
        self::assertFalse($this->object->isAbsolute('/php/net'));
    }

    public function testRouterQueryParams()
    {
        $params = ['id' => 12345, 'q' => 54321];
        $this->object->setParams($params, '/users/{id}');
        self::assertCount(1, $this->object->params);
        self::assertEquals('q', array_keys($this->object->params)[0]);
    }

    public function testSetUrl()
    {
        $this->object->initialize('', '', '');
        $this->object->setRouting([], 'users');
        self::assertEquals('/users/edit/12345', $this->object->setUrl('edit', ['id' => 12345], ''));
        self::assertEquals('/users', $this->object->setUrl('post', ['id' => 12345, 'test' => 'test']));
    }
}
