<?php


namespace FreedomSex\RestMachineBundle\Tests;


use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class BaseTestSetUp extends KernelTestCase
{
    /** @var RequestStack */
    public $requestStack;

    public function get($id)
    {
        return $this->getContainer()->get($id);
    }

    protected function setUp(): void
    {
        self::bootKernel();
        $request = new Request();
        $request->server->set('REMOTE_ADDR', '123.123.12.1');
        $this->requestStack = $this->get('request_stack');
        $this->requestStack->push($request);
    }

    public function loginUser(int $userId, array $roles = ['ROLE_USER'])
    {
        $this->requestStack->getCurrentRequest()->headers->set('X-Login', $userId);
        $this->requestStack->getCurrentRequest()->headers->set('X-Login-ID', $userId);
    }

}
